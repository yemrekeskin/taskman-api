const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb+srv://taskmandbuser:pJLPMsks15KW6dK3@cluster0.url0o.mongodb.net/taskmandb?retryWrites=true&w=majority', 
    { 
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => {
    console.log('Connected to the MongoDB.');
}).catch((e) => {
    console.log('Error while attempting to connect to the MongoDB.');
    console.log(e);
});


// prevent to deprectation wartning 
mongoose.set("useCreateIndex", true);
mongoose.set("useFindAndModify", false);

module.exports = {
    mongoose
};